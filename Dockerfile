FROM alpine

MAINTAINER PK ASWIN, pkaswin06@gmail.com

RUN apk update && \
    apk add zip && \
    apk add curl && \
    apk add git && \
    apk add wget 

CMD echo "Docker Image created - DATE"
